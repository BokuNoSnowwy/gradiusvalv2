﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreLifeManager : MonoBehaviour
{

    public int score;
    public int bestScore;
    public int remainingLife;


    public TextMeshProUGUI actualScoreText;
    public TextMeshProUGUI bestScoreText;
    public TextMeshProUGUI remainingLifeText;

    

    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddScore(int scoreToAdd)
    {
        score += scoreToAdd;
        int numberOfCaracter = score.ToString().Length;
        string preScore = "1P ";
        for (int i = 0; i < 7-numberOfCaracter; i++)
        {
            preScore += "0";
        }
        preScore += score.ToString();
        actualScoreText.text = preScore;
    }
}
