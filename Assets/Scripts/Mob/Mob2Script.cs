﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob2Script : MonoBehaviour
{
    public float speed;
    public Camera mainCamera;
    
    private Vector2 screenBounds;
    private GameObject player;
    private Rigidbody2D rgbd;

    public bool up;
    
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        mainCamera = Camera.main;
        screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height, mainCamera.transform.position.z));
        player = GameObject.FindGameObjectWithTag("Player");
        
    }

    // Update is called once per frame
    void Update()
    {
        //Vérifie si le mob est au dessus du player 
        if (transform.position.y > player.transform.position.y + 0.1f)
        {
            up = true;
            //fait le aller vers le bas
            rgbd.velocity = new Vector2(-1.5f,-1f) * speed ;
        }
        //Vérifie si le mob est en bas du player
        else if (transform.position.y < player.transform.position.y - 0.1f)
        {
            up = true;
            //fait le aller vers le haut
            rgbd.velocity = new Vector2(-1.5f,1f) * speed;
        }
        //Si le vaisseau est au milieu, lui fonce dessus
        else
        {
            up = false;
            rgbd.velocity = new Vector2(-1.5f,0f) * speed *1.7f;
        }

      
    }
}
