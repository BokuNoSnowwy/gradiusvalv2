﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyList : MonoBehaviour
{
    [System.Serializable]
    public class ListEnemy
    {
        public GroupEnemies[] enemyGroup;
       
    }

    [System.Serializable]
    public class GroupEnemies
    {
        public GameObject enemyPrefab;
        public float timeSpawn;
        public Transform spawnPoint;
    }
    public ListEnemy[] enemyList;

    public double time;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        time = Math.Round(Time.time, 1);

        for (int i = 0; i < enemyList.Length; i++)
        {
            for (int j = 0; j < enemyList[i].enemyGroup.Length; j++)
            {
                if (enemyList[i].enemyGroup[j].timeSpawn == time)
                { 
                    //Instantiate(enemyList[i].enemyGroup[j].enemyPrefab, enemyList[i].spawnPoint);
                    GameObject mob = enemyList[i].enemyGroup[j].enemyPrefab;
                    if (mob)
                    {
                        mob.SetActive(true);
                        mob.transform.position = enemyList[i].enemyGroup[j].spawnPoint.position;
                    }
                }
            }
        }
    }
    
}
