﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Mob1Script : MonoBehaviour
{
    public float speed;
    public Camera mainCamera;
    private Vector2 screenBounds;
    private GameObject player;

    public bool turnBack;
    private Rigidbody2D rgbd;
    
    public bonusListManager bonusListManager;
   
    public bool lastOne;
    
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        mainCamera = Camera.main;
        bonusListManager = FindObjectOfType<bonusListManager>();
       
        screenBounds = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height, mainCamera.transform.position.z));
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (player)
        {
            
            if (transform.position.x <= screenBounds.x * 0 -1)
            {
                rgbd.velocity = Vector2.zero;
                float distance = Vector2.Distance(transform.position, player.transform.position);
                transform.DOMove(new Vector2(screenBounds.x * 0, player.transform.position.y),distance/10).SetEase(Ease.Linear).OnComplete(()=>turnBack = true);
            }

            if (turnBack)
            {
                rgbd.velocity = Vector2.right * speed;
            }
            else
            {
                rgbd.velocity = Vector2.left * speed;
            }
        }
    }
    

    //Détruit le mob si il retourne a droite 
    private void OnBecameInvisible()
    {
        if (turnBack)
        {
            Destroy(gameObject);
        }
    }

    //Permet de faire drop un bonus si la derniere sphere du groupe de 4 est détruite
    private void OnDestroy()
    {
        bonusListManager.indexSphere += 1;
        if (lastOne)
        {
            if (bonusListManager.indexSphere == 4)
            {
                Instantiate(bonusListManager.bonusPrefa, transform.position, Quaternion.identity);
                bonusListManager.indexSphere = 0;
            }
            else
            {
                bonusListManager.indexSphere = 0;
            }
        }
    }
}
