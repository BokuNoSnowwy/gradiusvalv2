﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob3Script : MonoBehaviour
{
    public float speed;
    public float frequency;
    public float magnitude;
    public Vector3 pos, localScale;
    
    private Rigidbody2D rgbd;
    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
        localScale = transform.localScale;
        rgbd = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        pos -= transform.right * Time.deltaTime * speed;
        transform.position = pos + transform.up * Mathf.Sin(Time.time * frequency) * magnitude;
    }

}
