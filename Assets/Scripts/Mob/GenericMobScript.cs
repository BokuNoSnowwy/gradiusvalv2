﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericMobScript : MonoBehaviour
{
    public int life;
    public bool red;

    public Sprite spriteRed;
    public GameObject bonus;
    public ScoreLifeManager scoreManager;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        scoreManager = FindObjectOfType<ScoreLifeManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (red)
        {
            if (spriteRed)
            {
                GetComponent<SpriteRenderer>().sprite = spriteRed;
            }
        }
    }

    public void HitDamage()
    {
        life -= 1;
        if (life <= 0)
        {
            if (red)
            {
                Instantiate(bonus, transform.position, Quaternion.identity);
            }
            scoreManager.AddScore(100);
            Destroy(gameObject);
        }
    }
}
