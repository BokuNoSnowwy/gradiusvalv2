﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEditor;
using UnityEngine;

public class MissileScript : MonoBehaviour
{
    public Rigidbody2D rgbd;
    public Transform onInvisibleSpot;
    public float speed;

    public bool onSurface;
    public float distanceRay;
    
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!onSurface)
        {
            rgbd.velocity = new Vector2(0.7f,-1f) * speed;
        }
        else
        {
            Debug.DrawRay(transform.position,Vector3.right,Color.blue,2f);

            /*
            RaycastHit2D hitInfo =
                Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.right), distanceRay);
            
           // if(wallrightInfo.collider.)
           */
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ennemy"))
        {
            Debug.Log("Destroyed");
        }
        else if (other.gameObject.CompareTag("Ennemy"))
        {
            Debug.Log("OnSurface");
            onSurface = true;
        }
    }

    private void OnBecameInvisible()
    {
        //Si la balle est détruite, reset le timer d'attaque du player
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            GameObject.FindObjectOfType<PlayerController>().timerShot = 0;
        }
        transform.position = onInvisibleSpot.position;
        gameObject.SetActive(false);
    }
}
