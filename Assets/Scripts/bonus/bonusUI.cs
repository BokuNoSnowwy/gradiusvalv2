﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonusUI : MonoBehaviour
{
    public Sprite spriteSelected;
    public Sprite spriteUsed;
    public Sprite spriteBase;
    public Sprite spriteUsedSelected;

    public bool selected;
    public bool used;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (selected && !used)
        {
            if (spriteSelected)
            {
                GetComponent<SpriteRenderer>().sprite = spriteSelected;
            }
        }
        else if(used && !selected)
        {
            GetComponent<SpriteRenderer>().sprite = spriteUsed;
        }
        else if (selected  && used)
        {
            if (spriteUsedSelected)
            {
                GetComponent<SpriteRenderer>().sprite = spriteUsedSelected;
            }
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = spriteBase;
        }
    }
}
