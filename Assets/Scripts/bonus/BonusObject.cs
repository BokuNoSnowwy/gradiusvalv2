﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusObject : MonoBehaviour
{
    public bonusListManager listManager;
    public ScoreLifeManager scoreLifeManager;

    // Start is called before the first frame update
    void Start()
    {
        listManager = GameObject.FindGameObjectWithTag("ListManager").GetComponent<bonusListManager>();
        scoreLifeManager = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreLifeManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            listManager.IndexAdd();
            scoreLifeManager.AddScore(500);
            Destroy(gameObject);
        }
    }
}