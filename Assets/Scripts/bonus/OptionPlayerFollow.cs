﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;


public class OptionPlayerFollow : MonoBehaviour
{
    public PlayerMovement playerSpeed;
    public Transform objectToFollow;
    public float speed;
    public float spotDistanceXfromPlayer = 0.65f;
    public float limitMoving = 2.7f;


    // Start is called before the first frame update
    void Start()
    {
        playerSpeed = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        speed = playerSpeed.speed;
    }

    void FixedUpdate()
    {
        if (objectToFollow)
        {
            Vector3 SpotOption = new Vector3(objectToFollow.position.x - spotDistanceXfromPlayer,
                objectToFollow.position.y,
                objectToFollow.position.z);

            if (Vector3.Distance(SpotOption, transform.position) >= limitMoving)
            {
                Vector2 toTarget = SpotOption - transform.position;
                transform.Translate(toTarget * speed * Time.deltaTime);
            }
        }
    }
}