﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bonusListManager : MonoBehaviour
{
    public bonusUI[] bonusList;
    public int indexBonus = 0;
    public GameObject player;
    public GameObject bonusPrefa;
    public int indexSphere;

    
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) )
        {
            if (indexBonus != 0)
            {
                switch (indexBonus)
                {
                    case 1 :
                        player.GetComponent<PlayerMovement>().SpeedUp();
                        break;
                    case 2 :
                        bonusList[1].used = true;
                        player.GetComponent<PlayerController>().SpawnMissile();
                        break;
                    case 3 :
                        bonusList[2].used = true;
                        bonusList[3].used = false;
                        player.GetComponent<PlayerController>().ChangeAmmoDouble();
                        break;
                    case 4 :
                        bonusList[3].used = true;
                        bonusList[2].used = false;
                        player.GetComponent<PlayerController>().ChangeAmmoLaser();
                        break;
                    case 5 :
                        //Le bonus d'option devient Used seulement après la deuxieme utilisation
                        if (player.GetComponent<PlayerController>().nbOptions == 0)
                        {
                            player.GetComponent<PlayerController>().SpawnOption();
                        }
                        else
                        {
                            bonusList[4].used = true;
                            player.GetComponent<PlayerController>().SpawnOption();
                        }
                        break;
                    case 6 :
                        bonusList[5].used = true;
                        player.GetComponent<PlayerController>().SpawnBarrier();
                        break;
                    default :

                        break;
                }
            }
            ResetBonus();
        }
    }

    public void IndexAdd()
    {
        indexBonus += 1;
        if (indexBonus == 7)
        {
            indexBonus = 1;
        }

        //Met a jour l'affichage des sprites 
        if (indexBonus != 0)
        {
            for (int i = 0; i < bonusList.Length; i++)
            {
                if (i == indexBonus)
                {
                    bonusList[i-1].selected = true;
                }
                //Gestion du dernier index par rapport au bonusList[i-1] d'en haut
                else if (indexBonus == 6)
                {
                    bonusList[5].selected = true;
                    bonusList[4].selected = false;
                }
                else
                {
                    bonusList[i].selected = false;
                }
            }   
        }
    }
    
    public void ResetBonus()
    {
        indexBonus = 0;
        foreach (var bonus in bonusList)
        {
            bonus.selected = false;
        }
    }
}
