﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldScript : MonoBehaviour
{
    public bool activated;
    public int remainingHit;
    public Transform invisibleSpot;
    public Transform shotSpotPlayer;

    public Sprite blueShield;
    public Sprite orangeShield;
    // Start is called before the first frame update
    void Start()
    {
        invisibleSpot = GameObject.FindGameObjectWithTag("OutOfCameraSpot").transform;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (activated)
        {
            transform.position = shotSpotPlayer.position;
        }
        else
        {
            transform.position = invisibleSpot.position;
        }
       
    }

    public void ResetShield()
    {
        GetComponent<SpriteRenderer>().sprite = blueShield;
        activated = true;
        remainingHit = 5;
    }

    public void HitTaken()
    {
        if (activated && remainingHit >= 1)
        {
            remainingHit -= 1;
            if (remainingHit == 1)
            {
                GetComponent<SpriteRenderer>().sprite = orangeShield;
            }
            Debug.Log("Shield Remaining : " + remainingHit);
        }
        else
        {
            transform.position = invisibleSpot.position;
            activated = false;
            gameObject.SetActive(false);
        }
    }
}
