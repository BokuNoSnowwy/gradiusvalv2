﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float timerShot;
    public float timerShotReset;

    public Transform shotSpot;
    public ListOfShot[] shotListv2 = new ListOfShot[3];

    public int indexFire;
    public int indexTimeOfAmmo;

    public int nbOptions;
    public GameObject[] listOptions = new GameObject[2];
    
    //Missile
    public bool missileOnable;
    public GameObject missile;
    
    //Shield 
    public ShieldScript shield;

    [System.Serializable]
    public class ListOfShot
    {
        public GameObject[] shotList = new GameObject[2];
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        timerShot -= Time.deltaTime;

        if (Input.GetKey(KeyCode.F))
        {
            if (timerShot <= 0)
            {
                if (indexTimeOfAmmo != 1)
                {
                    if (CountNonActiveShot() > 0)
                    {
                        Shoot();
                        timerShot = timerShotReset;
                    }
                }
                else
                {
                    Shoot();
                }

                if (missileOnable && missile.activeSelf == false)
                {
                    missile.SetActive(true);
                    missile.transform.position = new Vector3(transform.position.x, transform.position.y - 0.3f,
                        transform.position.z);
                }
            }
        }

        //Modifie en temps reel l'index
        indexFire = CountNonActiveShot() - 1;

        //Si l'objet est une Option, prend les valeurs du joueur
        if (gameObject.CompareTag("Option"))
        {
            indexFire = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().indexFire;
            indexTimeOfAmmo = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().indexTimeOfAmmo;
            missileOnable = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().missileOnable;
                
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (CompareTag("Player"))
        {
            if (other.CompareTag("Ennemy"))
            {
                if (!shield.activated && shield.remainingHit <= 0)
                {
                    if (other.gameObject.CompareTag("Ennemy"))
                    {
                        SceneManager.LoadScene(0);
                    }
                }
                else
                {
                    shield.HitTaken();
                }
            }
        }
    }


    private int CountNonActiveShot()
    {
        int returnValue = 0;
        foreach (var shot in shotListv2[indexTimeOfAmmo].shotList)
        {
            if (!shot.activeSelf)
            {
                returnValue += 1;
            }
        }

        return returnValue;
    }

    private void Shoot()
    {
        if (indexFire >= 0)
        {
            if (indexTimeOfAmmo != 1)
            {
                if (shotListv2[indexTimeOfAmmo].shotList[indexFire].gameObject.activeSelf)
                {
                    indexFire += 1;
                }

                GameObject shot = shotListv2[indexTimeOfAmmo].shotList[indexFire].gameObject;
                shot.SetActive(true);
                shot.transform.position = shotSpot.position;
            }
            else
            {
                //Ce tir n'a pas besoin de prendre en compte l'indexFire
                GameObject shot1 = shotListv2[indexTimeOfAmmo].shotList[0].gameObject;
                if (!shot1.activeSelf)
                {
                    shot1.SetActive(true);
                    shot1.transform.position = shotSpot.position;
                }

                GameObject shot2 = shotListv2[indexTimeOfAmmo].shotList[1].gameObject;
                if (!shot2.activeSelf)
                {
                    shot2.SetActive(true);
                    shot2.transform.position = shotSpot.position;
                }
            }
        }
    }

    public void SpawnMissile()
    {
        missileOnable = true;
    }

    //Ce bonus ne doit pas etre présent dans les script PlayerController présent dans Option
    public void SpawnOption()
    {
        if (CompareTag("Player"))
        {
            if (listOptions != null)
            {
                listOptions[nbOptions].transform.position = transform.position;
                listOptions[nbOptions].SetActive(true);
                nbOptions += 1;
            }
        }
    }

    public void SpawnBarrier()
    {
        if (CompareTag("Player"))
        {
            shield.transform.position = shotSpot.position;
            shield.gameObject.SetActive(true);
            shield.ResetShield();
        }
    }

    public void ChangeAmmoDouble()
    {
        indexTimeOfAmmo = 1;
    }

    public void ChangeAmmoLaser()
    {
        indexTimeOfAmmo = 2;
    }
}