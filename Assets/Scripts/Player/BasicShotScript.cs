﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicShotScript : MonoBehaviour
{
    public Rigidbody2D rgbd;
    public Transform onInvisibleSpot;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        if (!onInvisibleSpot)
        {
            onInvisibleSpot = GameObject.FindGameObjectWithTag("OutOfCameraSpot").transform;
        }
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //Si c'est le tir double qui est incliné la balle va direction haut gauche 45°
        if (transform.rotation.z > 0)
        {
            Debug.Log("45 degre");
            //rgbd.AddForce(Vector3.forward * speed);
            rgbd.velocity = Vector2.one * speed;
        }
        else
        {
            //Sinon la balle va tout droit
            rgbd.velocity = Vector2.right * speed;
        }
       
    }
    
    private void OnBecameInvisible()
    {
       ResetShot();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ennemy"))
        {
            if (other.GetComponent<Mob1Script>())
            {
                other.GetComponent<GenericMobScript>().HitDamage();
            }
            else
            {
                other.GetComponent<GenericMobScript>().HitDamage();
            }

            if (!CompareTag("Laser"))
            {
                ResetShot();
            }
        }
    }

    public void ResetShot()
    {
        //Si la balle est détruite, reset le timer d'attaque du player
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            GameObject.FindObjectOfType<PlayerController>().timerShot = 0;
        }
        transform.position = onInvisibleSpot.position;
        gameObject.SetActive(false);
    }
}
